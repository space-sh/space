# Contributing

Code contributions are welcome via [GitLab merge requests](https://gitlab.com/space-sh/space/merge_requests).

In order to submit patches, create a fork of Space and apply your changes to a new branch. After the new bug fix or feature is ready, go to the _Merge Request_ tab in your GitLab fork and create a new merge request. In the _Merge Request_ page, choose your forked project's branch as the source and the _space-sh/space_ project as the destination and then submit the merge request.

For issues and feature requests, please refer to the project [GitLab Issues](https://gitlab.com/space-sh/space/issues) page.

