---
prev: /getting-help/
prev_title: "Getting help"
next: /troubleshooting/
next_title: "Troubleshooting"
title: Contributing
giturl: gitlab.com/space-sh/space
editurl: /edit/master/doc/contributing/index.md
weight: 1200
icon: "<b>12. </b>"
---

# Contributing

Contributions are welcome via [GitLab merge requests](https://gitlab.com/space-sh/space/merge_requests).
