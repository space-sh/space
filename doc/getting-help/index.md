---
prev: /references/
prev_title: "References"
next: /contributing/
next_title: "Contributing"
title: Getting help
giturl: gitlab.com/space-sh/space
editurl: /edit/master/doc/getting-help/index.md
weight: 1100
icon: "<b>11. </b>"
---

# Getting help

For issues and feature requests, please refer to the project page [GitLab Issues](https://gitlab.com/space-sh/space/issues).  
Visit https://space.sh for up to date news, information and tutorials.
